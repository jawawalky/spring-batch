/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex15;

import org.springframework.batch.core.explore.support.JobExplorerFactoryBean;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The batch configuration.
 * 
 * @author Franz Tost
 *
 */
@Configuration
public class ControllerConfiguration {

	// fields
	// ........................................................................
	
	// TODO
	//
	//  o Autowire
	//
	//      o a data source,
	//      o the job repository,
	//      o the job registry and
	//      o the job launcher
	//
	
	// constructors
	// ........................................................................

	// methods
	// ........................................................................

	@Bean public JobExplorerFactoryBean jobExplorerFactory() {
		
		JobExplorerFactoryBean jobExplorerFactory = null;
		
		// TODO
		//
		//  o Create an appropriate 'JobExplorerFactoryBean', which operates
		//    on the given data source.
		//
		//  o Return the configured 'JobExplorerFactoryBean'.
		//
		
		return jobExplorerFactory;
		
	}
	
	@Bean public JobOperator jobOperator() {
		
		// TODO
		//
		//  o Create a correctly configured 'JobOperator'.
		//
		//    Hint: Use the class
		//          'org.springframework.batch.core.launch.support.SimpleJobOperator'.
		//
		//  o Return the configured 'JobOperator' instance.
		//
		
		return null;
			
	}
	
}

Getting Started with Spring Batch
=================================

  This demo uses Spring Boot, which makes a lot of things easier, but also
  hides a lot of details, which are important, if you really want to use
  Spring Batch. This demo is intended to get you into Spring Batch with
  the least possible effort. The following examples will dig deep under
  the surface, so you learn profoundly about the Spring Batch Framework.
  

A) The Batch Configuration
==========================

  'BatchConfiguration':
  ---------------------
  
  1) Make this class a configuration class by annotating it with
     'org.springframework.context.annotation.Configuration'.
     
  2) Enable batch processing with the appropriate annotation.
  
  3) Enable auto configuration with the appropriate annotation.
  
  
  'fields':
  ---------
  
  4) Autowire a 'JobBuilderFactory', which can be used to construct
     the batch job.
     
  5) Autowire a 'StepBuilderFactory', which can be used to construct
     the steps of a batch job.
     

  'step1()':
  ----------
  
  6) Declare this method as a bean method.
     
       Hint: Use an appropriate annotation.
       
  7) Create a step called 'step1' using the step builder factory.
  
  8) Add a tasklet instance by creating an anonymous sub-class of
     the class 'org.springframework.batch.core.step.tasklet.Tasklet'.
     
  9) Implement the 'execute(...)' method of that tasklet by returning 'null'.
  
  10) Build the step instance.
     
       Hint: The step builder uses a 'fluent' API.
       
  
  'job(Step)':
  ------------
  
  11) Declare this method as a bean method.
  
        Hint: Use an appropriate annotation.
        
  12) Create a job called 'job1' using the job builder factory.
  
  13) Add an incrementer of type 'RunIdIncrementer'.
  
  14) Set the first step as the start step.
  
  15) Build the job instance.
  
        Hint: The job builder uses a 'fluent' API.

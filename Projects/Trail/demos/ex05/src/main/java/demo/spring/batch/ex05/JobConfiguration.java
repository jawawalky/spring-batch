/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex05;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.xml.StaxEventItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

/**
 * The configuration of the batch job.
 * 
 * @author Franz Tost
 *
 */
@Configuration
public class JobConfiguration {

    // fields
	// ........................................................................
	
	@Autowired
	private JobBuilderFactory jobBuilders;
 
	@Autowired
	private StepBuilderFactory stepBuilders;
 
    // methods
	// ........................................................................
	
	@Bean
	public Job job() {
		
		return
			this.jobBuilders
				.get("job")
				.start(step())
				.preventRestart()
				.build();
	}
 
	@Bean
	public Step step() {
		
		return
			this.stepBuilders
				.get("step")
				.<Book, Book>chunk(1)
				.reader(csvItemReader())
				.processor(itemProcessor())
				.writer(xmlItemWriter())
				.build();
		
	}
	
	@Bean
	public FlatFileItemReader<Book> csvItemReader() {
		
		DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
		lineTokenizer.setNames(new String[] {"isbn", "author", "title", "price"});
		
		BeanWrapperFieldSetMapper<Book> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
		fieldSetMapper.setTargetType(Book.class);
		
		DefaultLineMapper<Book> lineMapper = new DefaultLineMapper<>();
		lineMapper.setLineTokenizer(lineTokenizer);
		lineMapper.setFieldSetMapper(fieldSetMapper);
		
		FlatFileItemReader<Book> itemReader = new FlatFileItemReader<Book>();
		itemReader.setResource(new FileSystemResource("input/books.csv"));
		itemReader.setLineMapper(lineMapper);
		
		return itemReader;
		
	}
 
	@Bean
	public ItemProcessor<Book, Book> itemProcessor() {
		
		return new BookProcessor();
		
	}
 
	@Bean
	public ItemWriter<Book> xmlItemWriter() {
		
		StaxEventItemWriter<Book> itemWriter = new StaxEventItemWriter<Book>();
		itemWriter.setResource(new FileSystemResource("output/books.xml"));
		itemWriter.setMarshaller(reportMarshaller());
		itemWriter.setRootTagName("book");
		return itemWriter;
		
	}
	
	@Bean
	public Jaxb2Marshaller reportMarshaller() {
		
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();		
		marshaller.setClassesToBeBound(Book.class);
		return marshaller;
		
	}
	
}

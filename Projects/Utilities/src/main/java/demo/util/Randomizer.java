/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2012 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.util;

import java.util.Random;

/**
 * A utility class for randomized data.
 */
public final class Randomizer {

	// constants
	// ........................................................................

	/**
	 * A random generator.
	 */
	private static final Random RANDOM = new Random();

	// fields
	// ........................................................................

	// constructors
	// ........................................................................

	/**
	 * A private constructor.
	 */
	private Randomizer() {}

	// methods
	// ........................................................................
	
	/**
	 * Returns a random <code>int</code> value in the interval [0, n).
	 * 
	 * @param n the upper limit of the interval.
	 * 
	 * @return The random value.
	 */
	public static int nextInt(final int n) {
		
		return RANDOM.nextInt(n);
		
	}

	/**
	 * Returns a random character from a character set.
	 * 
	 * @param charSet
	 *        the character set.
	 * 
	 * @return The random value.
	 */
	public static char nextChar(final String charSet) {
		
		return charSet.charAt(nextInt(charSet.length()));
		
	}

	/**
	 * Returns a random lower-case character.
	 * 
	 * @return The random value.
	 */
	public static char nextLowerCaseChar() {
		
		return nextChar("abcdefghijklmnopqrstuvwxyz");
		
	}

	/**
	 * Returns a random upper-case character.
	 * 
	 * @return The random value.
	 */
	public static char nextUpperCaseChar() {
		
		return nextChar("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
		
	}

	/**
	 * Returns a random string composed of the specified characters.
	 * 
	 * @param charSet
	 *        the characters, of which the string should be composed.
	 *        
	 * @param length
	 *        the length of the random string.
	 *        
	 * @return The random value.
	 */
	public static String nextString(final String charSet, final int length) {
		
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < length; i++) {
			
			sb.append(nextChar(charSet));
			
		} // for
		
		return sb.toString();
		
	}

	/**
	 * Returns a random enum value.
	 * 
	 * @param enumType
	 *        the enum type.
	 * 
	 * @return The random enum value.
	 */
	public static <T extends Enum<T>> T nextEnum(
		final Class<T> enumType
	) {
		
		T[] enumValues = enumType.getEnumConstants();
		return enumValues[RANDOM.nextInt(enumValues.length)];
		
	}

}

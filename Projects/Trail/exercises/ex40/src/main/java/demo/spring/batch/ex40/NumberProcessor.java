/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex40;

/**
 * A simple item processor, which calculates the square of a number.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Let the class implement the interface
 *    'org.springframework.batch.item.ItemProcessor'.
 */
public class NumberProcessor {
	
	// methods
	// ........................................................................
	
	// TODO
	//
	//  o Implement the method 'process(Integer)'.
	//
	//  o Return the square of the passed number.
	//

}

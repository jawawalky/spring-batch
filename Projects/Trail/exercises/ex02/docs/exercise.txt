XML-based Configuration
=======================


A) The Item Processor
=====================

  'BookProcessor':
  ----------------
  
  1) Let the 'BookProcessor' implement the 'ItemProcessor' interface.
  
  2) Override the 'process(...)' method.
  
  3) Print the book on the console.
  
  4) Return the book instance.

  
B) The Batch Configuration
==========================

  'job.xml':
  ----------
  
  1) Add a bean instance called 'itemProcessor' of type
     'demo.spring.batch.ex02.BookProcessor'.
     
  2) Complete the following bean definition by substituting the '{*}'
     place holders by the correct value.
     
     The following values must be used
     
       o 'org.springframework.batch.item.file.mapping.DefaultLineMapper'
       o 'org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper'
       o 'file:input/books.csv'
       o 'isbn,author,title,price'
       o 'org.springframework.batch.item.file.transform.DelimitedLineTokenizer'
       o 'org.springframework.batch.item.file.FlatFileItemReader'
       o 'book'
     
     <bean
         id    = "csvItemReader"
         class = "{1}">
        
         <property name="resource" value="{2}" />
        
         <property name="lineMapper">
             <bean class="{3}">
                 <property name="lineTokenizer">
                     <bean class="{4}">
                         <property name="names" value="{5}" />
                     </bean>
                 </property>
                 <property name="fieldSetMapper">
                     <bean class="{6}">
                         <property name="prototypeBeanName" value="{7}"/>
                     </bean>
                 </property>
             </bean>
         </property>
     </bean>
     
  3) Complete the following bean definition by substituting the '{*}'
     place holders by the correct value.
     
     The following values must be used
     
       o 'file:output/books.xml'
       o 'org.springframework.batch.item.xml.StaxEventItemWriter'
       o 'book'
       o 'reportMarshaller'
       
     <bean
         id    = "xmlItemWriter"
         class = "{1}">
        
         <property name="resource"    value="{2}"/>
         <property name="marshaller"  ref="{3}"/>
         <property name="rootTagName" value="{4}"/>
        
     </bean>
     
  4) Create a batch job with the ID 'job'.
  
  5) Add one step with the ID 'step1'.
  
  6) Let that step perform a tasklet containing a chunk consisting of
     
       o the 'csvItemReader',
       o the 'itemProcessor' and
       o the 'xmlItemWriter'
       
  7) Let the commit interval be 10.
  
  Hint:
  
    The general structure of a job definition is
    
      <batch:job id="...">
          <batch:step id="...">
              <batch:tasklet>
                  <batch:chunk reader="..." processor="..." writer="..." commit-interval="..."/>
              </batch:tasklet>
          </batch:step>
      </batch:job>


C) Running the Batch Job
========================

  'DemoApp':
  ----------
  
  'runDemo()':
  ------------
  
  1) Get the 'JobLauncher' instance from the bean context.
  
  2) Get the 'Job' instance 'job' from the bean context.
  
  3) Use the 'JobLauncher' to run the job.
  
  4) Check the status of the job execution.

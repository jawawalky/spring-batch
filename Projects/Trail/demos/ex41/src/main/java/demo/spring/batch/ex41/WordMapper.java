/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex41;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

/**
 * A row manager maps the values of a result set row to {@link Word} objects.
 * 
 * @author Franz Tost
 *
 */
public class WordMapper implements RowMapper<Word> {

	// methods
	// ........................................................................

	@Override
	public Word mapRow(
		final ResultSet resultSet,
		final int       rowIndex
	) throws SQLException {

		return new Word(resultSet.getInt("id"), resultSet.getString("text"));

	}

}

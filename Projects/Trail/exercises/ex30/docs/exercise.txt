Conditional Flows
=================


A) The Tasklet Classes
======================

  'Tasklet1':
  -----------
  
  'execute(...)':
  ---------------
  
  1) Let this tasklet fail with a probability of 50%, i.e. throw
     an exception with 50% probability.
     
     Hint: Use 'Demo.nextInt(...)'
     
     
B) The Job Configuration
========================

  'job.xml':
  ----------
  
  1) Define three beans for
     
       o 'Tasklet1',
       o 'Tasklet2' and
       o 'Tasklet3'
   
       
  '<job id="job" ...>...</job>':
  ------------------------------
  
  2) Define three steps 'step1', 'step2' and 'step3', calling
     'Tasklet1', 'Tasklet2' and 'Tasklet3' respectively.
     
  3) Create a flow, which first calls 'step1' and then 'step2',
     if 'step1' returns without an error. If 'step1' fails, then
     'step3' should be invoked instead of 'step2'.

Guide
=====

A) Building the Eclipse Project
===============================

  1) Open a terminal window.
  
  2) Go to the project folder.
  
  3) Run the command
  
       gradle cleanEclipse eclipse
      
      
B) Building the Project
=======================

  1) Open a terminal window.
  
  2) Go to the project folder.
  
  3) Run the command
  
       gradle clean build
      
      
C) Running the Demo
===================

  1) Remove the file 'data/output.txt'.
  
  2) Run the demo by simply starting 'DemoApp' inside your Eclipse IDE.
  
  3) Observe that the batch job fails.
  
  4) Refresh the project an check, how much of the output file has been
     written.
     
  5) Run the batch job again and see that it still fails.
  
  6) Refresh the project an check that the output file has not changed.
  
  7) Remove line 39 of 'data/input.txt'. It is the line, which makes the job
     fail.
     
  8) Rerun the job. It fails again, but at another line.
     
  9) Refresh the project an check, how much of the output file has been
     written.
     
  10) Remove line 64 of 'data/input.txt'. It is the line, which makes the job
     fail.
     
  11) Rerun the job and see it succeed.
     
  12) Refresh your project and check the output file.

  13) What happens, if you try to restart the job? Why?
  
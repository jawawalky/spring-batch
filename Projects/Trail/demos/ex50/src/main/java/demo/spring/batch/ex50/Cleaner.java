/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex50;

import java.io.File;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import demo.util.Demo;

/**
 * The cleaner is a tasklet, which removes all files from a certain folder.
 * 
 * @author Franz Tost
 *
 */
public class Cleaner implements Tasklet {
	
	// fields
	// ........................................................................
	
	private String folder;

	// constructors
	// ........................................................................
	
	public Cleaner(final String folder) {
		
		super();
		
		this.folder = folder;
		
	}

	// methods
	// ........................................................................

	@Override
	public RepeatStatus execute(
		final StepContribution contribution,
		final ChunkContext     chunkContext
	) throws Exception {

		File dir = new File(this.folder);
		
		for (File file : dir.listFiles()) {
			
			Demo.log("Removing %s ...", file.getName());
			file.delete();
			
		} // for
		
		return RepeatStatus.FINISHED;
		
	}

}

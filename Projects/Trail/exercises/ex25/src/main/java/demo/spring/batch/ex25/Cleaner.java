/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex25;

/**
 * The cleaner is a tasklet, which removes all files from a certain folder.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Let the class implement the interface
 *    'org.springframework.batch.core.step.tasklet.Tasklet'
 */
public class Cleaner {
	
	// fields
	// ........................................................................
	
	private String folder;

	// constructors
	// ........................................................................
	
	public Cleaner(final String folder) {
		
		super();
		
		this.folder = folder;
		
	}

	// methods
	// ........................................................................
	
	// TODO
	//
	//  o Implement the method
	//    'execute(StepContribution, ChunkContext chunkContext)'.
	//
	//  o Delete all files inside the desired folder.
	//
	//  o Return an appropriate repeat status.
	//

}

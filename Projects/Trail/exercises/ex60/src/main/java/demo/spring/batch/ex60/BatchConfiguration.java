/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex60;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;

/**
 * The batch configuration.
 * 
 * @author Franz Tost
 *
 */
@Configuration
@EnableBatchProcessing
@EnableAutoConfiguration
public class BatchConfiguration {

	// fields
	// ........................................................................

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	// constructors
	// ........................................................................

	// methods
	// ........................................................................

	@Bean public NumberReader    reader()    { return new NumberReader();    }
	@Bean public NumberProcessor processor() { return new NumberProcessor(); }
	@Bean public NumberWriter    writer()    { return new NumberWriter();    }
	
	@Bean public TaskExecutor    executor() {
		
		// TODO
		//
		//  o Create a 'TaskExecutor', which is based on a thread pool.
		//
		//    Hint: Choose an appropriate implementation of the interface
		//          'TaskExecutor'.
		//
		//  o Set the core pool size to 5.
		//
		//  o Set the maximum pool size to 5.
		//
		//  o Set the bean name to 'executor'.
		//
		//  o Return the executor.
		//
		
		return null;
		
	}

	@Bean public Step step() {
		
		// TODO
		//
		//  o Complete the job definition, different chucks are run by
		//    the task executor defined above.
		//
		//    Hint: Find the right method of the step builder factory or
		//          of one of its created objects.
		//
		
		return
			this.stepBuilderFactory
				.get("step")
				.<Integer, Integer>chunk(5)
				.reader(this.reader())
				.processor(this.processor())
				.writer(this.writer())
				.build();
		
	}

	@Bean public Job job(final Step step) throws Exception {
		
		return
			this.jobBuilderFactory
				.get("job")
				.incrementer(new RunIdIncrementer())
				.listener(new StopClock())
				.start(this.step())
				.build();
		
	}
	
}

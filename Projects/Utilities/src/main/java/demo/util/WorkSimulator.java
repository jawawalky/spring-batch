/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2019 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.util;

/**
 * An abstract base class for work simulations.
 * 
 * @author Franz Tost
 *
 */
public abstract class WorkSimulator {

    // fields
	// ........................................................................
	
	private long minWorkTime;
	
	private long maxWorkTime;
	
    // methods
	// ........................................................................
	
	protected void simulateWork() {
		
		Threading.sleep(this.minWorkTime, this.maxWorkTime);
		
	}

	public long getMinWorkTime() { return this.minWorkTime; }
	
	public void setMinWorkTime(long minWorkTime) {
		
		this.minWorkTime = minWorkTime;
		
		if (this.minWorkTime > this.maxWorkTime) {
			
			this.maxWorkTime = this.minWorkTime;
			
		} // if
		
	}

	public long getMaxWorkTime() { return this.maxWorkTime; }
	
	public void setMaxWorkTime(long maxWorkTime) {
		
		if (maxWorkTime >= this.minWorkTime) {
			
			this.maxWorkTime = maxWorkTime;
			
		} // if
		
	}

}

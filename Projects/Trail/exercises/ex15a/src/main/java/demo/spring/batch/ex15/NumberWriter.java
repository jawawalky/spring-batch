/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex15;

import static java.util.stream.Collectors.joining;

import java.util.List;

import org.springframework.batch.item.ItemWriter;

import demo.util.Demo;

/**
 * A writer, which prints the processed numbers on the console.
 * 
 * @author Franz Tost
 *
 */
public class NumberWriter implements ItemWriter<Integer> {

	@Override
	public synchronized void write(
		final List<? extends Integer> numbers
	) throws Exception {
		
		Demo.log("W [%s] > %s",
			Thread.currentThread().getName(),
			numbers.stream().map(n -> String.valueOf(n)).collect(joining(", "))
		);
		
	}

}

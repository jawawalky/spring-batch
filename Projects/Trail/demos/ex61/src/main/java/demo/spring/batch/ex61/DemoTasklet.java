/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex61;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import demo.util.Demo;

/**
 * A tasklet, simulating work.
 * 
 * @author Franz Tost
 *
 */
public class DemoTasklet implements Tasklet {
	
	// fields
	// ........................................................................
	
	private String name;
	
	private long   sleepTime;

	// constructors
	// ........................................................................
	
	public DemoTasklet(final String name, final long sleepTime) {
		
		super();
		
		this.name      = name;
		this.sleepTime = sleepTime;
		
	}

	// methods
	// ........................................................................

	@Override
	public RepeatStatus execute(
		final StepContribution contribution,
		final ChunkContext     chunkContext
	) throws Exception {
		
		Demo.log("Running tasklet %s in [%s] ...", this.name, Thread.currentThread().getName());
		Demo.sleep(this.sleepTime);
		Demo.log("Finished tasklet %s.", this.name);

		return RepeatStatus.FINISHED;
		
	}

}

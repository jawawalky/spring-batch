/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex15;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import demo.util.Demo;

/**
 * This demo shows you, how you can stop a job decently.
 * 
 * @author Franz Tost
 *
 */
public class DemoApp {

	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	/**
	 * Runs the demo.
	 * 
	 * @param args
	 *        no arguments needed.
	 */
	public static void main(final String[] args) {
		
		Demo.log("Running demo ...");
		
		ApplicationContext context =
			new AnnotationConfigApplicationContext(
				DatabaseConfiguration.class,
				BatchConfiguration.class
			);

		JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
		Job         job         = (Job)         context.getBean("job");

		try {

			JobExecution execution = jobLauncher.run(job, new JobParameters());
			Demo.log("Exit Status : " + execution.getStatus());

		} // try
		catch (Exception e) {
			
			Demo.log(e);
			
		} // catch

		Demo.log("Finished.");

	}
	
}

/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex60;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

import demo.util.Demo;

/**
 * A job execution listener, which measures the duration of a job.
 * 
 * @author Franz Tost
 *
 */
public class StopClock implements JobExecutionListener {
	
	// fields
	// ........................................................................
	
	private long startTime;

	// methods
	// ........................................................................

	@Override
	public void beforeJob(final JobExecution jobExecution) {
		
		this.startTime = System.currentTimeMillis();
		
	}

	@Override
	public void afterJob(final JobExecution jobExecution) {
		
		long endTime = System.currentTimeMillis();
		Demo.log("Job %d took %d ms.", jobExecution.getId(), (endTime - this.startTime));

	}
	
}

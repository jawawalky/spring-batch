/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex80;

import java.util.Date;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import demo.util.Demo;

/**
 * A bean, which contains the scheduled jobs.
 * 
 * @author Franz Tost
 *
 */
@Component
public class Tasks {
	
	// fields
	// ........................................................................

	@Autowired private JobLauncher jobLauncher;

	@Autowired private Job         job;
	
	// methods
	// ........................................................................

	@Scheduled(cron="*/5 * * * * *")
	public void runJob() {
		
		try {
			
			// Note:
			//
			//  Only if the job parameters differ, a new job instance will be
			//  created. So we pass in a time-stamp, which makes every
			//  parameter instance different.
			//  If you use the same job parameters for each scheduled run, then
			//  the completed steps must be restartable.
			//
			JobParametersBuilder parametersBuilder = new JobParametersBuilder();
			parametersBuilder.addDate("now", new Date());
			
			JobExecution execution =
				this.jobLauncher.run(this.job, parametersBuilder.toJobParameters());
			
			Demo.log(
				"Job %d exited with status %s.",
				execution.getJobId(),
				execution.getExitStatus().getExitCode()
			);
			
		} // try
		catch (Exception e) {
			
			Demo.log(e);
			
		} // catch
		
	}
	
}

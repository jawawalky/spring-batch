/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex03;

import javax.sql.DataSource;

import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * A configuration for the general context.
 * 
 * @author Franz Tost
 *
 */
@Configuration
public class ContextConfiguration {

    // fields
	// ........................................................................
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private PlatformTransactionManager transactionManager;
	
    // methods
	// ........................................................................
	
	@Bean
	public JobRepository jobRepository() {
		
		try {
			
			JobRepositoryFactoryBean repository = new JobRepositoryFactoryBean();
			repository.setDataSource(this.dataSource);
			repository.setTransactionManager(this.transactionManager);
			repository.setDatabaseType("derby");
			
			return repository.getObject();
			
		} // try
		catch (Exception e) {
			
			throw new RuntimeException(e);
			
		} // catch
		
	}
	
	@Bean
	public SimpleJobLauncher jobLauncher() {
		
		SimpleJobLauncher launcher = new SimpleJobLauncher();
		launcher.setJobRepository(jobRepository());
		
		return launcher;
		
	}
	
}

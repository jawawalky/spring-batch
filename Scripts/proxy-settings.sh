#!/bin/sh

#  Proxy
#  =====
#
# Sometimes it might be hard to find the correct syntax to by-pass a proxy.
# Here are some suggestions you may try.
#


# A) Environment Variables
# ------------------------
# 
# Some programs use the environment variables
#
#  o HTTP_PROXY and
#  o HTTPS_PROXY
#
# So you can try to set them as follows
#
#  export HTTP_PROXY="<protocol>://[<username>:<password>@]<host>[:<port>]"
#  export HTTPS_PROXY="<protocol>://[<username>:<password>@]<host>[:<port>]"
#

# HTTP/HTTPS
#
# In case of an HTTP proxy that would be
#

#export HTTP_PROXY="http://[<username>:<password>@]<host>[:<port>]"
#export HTTPS_PROXY="https://[<username>:<password>@]<host>[:<port>]"

# SOCKS
#
# In case of a SOCKS proxy that would be
#

#export HTTP_PROXY="socks5://<host>[:<port>]"
#export HTTPS_PROXY="socks5://<host>[:<port>]"


# B) CURL Options
# ---------------
#
# You can also try to pass the proxy settings directly to the CURL command.
# Either use
#
#  curl --proxy <protocol>://[<username>:<password>@]<host>[:<port>] <url>
#
# or
#
#  curl --proxy <protocol>://<host>[:<port>] [--proxy-user <username>:<password>] <url>
#

# HTTP/HTTPS
#
# In case of an HTTP proxy that would be one of the following
#

#export CURL_PROXY="--proxy http://[<username>:<password>@]<host>[:<port>]"
#export CURL_PROXY="--proxy http://<host>[:<port>] [--proxy-user <username>:<password>]"

# SOCKS
#
# In case of a SOCKS proxy that would be
#

#export CURL_PROXY="--socks5 http://<host>[:<port>] [--proxy-user <username>:<password>]"


export CURL_COMMAND="curl"

if [ ! -z "$CURL_PROXY" ]
  then
    export CURL_COMMAND="$CURL_COMMAND $CURL_PROXY"
fi


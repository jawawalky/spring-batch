/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex80;

import org.springframework.boot.SpringApplication;

/**
 * This demo shows you, how you can schedule a job.
 * 
 * @author Franz Tost
 *
 */
public class DemoApp {

	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	/**
	 * Runs the demo.
	 * 
	 * @param args
	 *        no arguments needed.
	 */
	public static void main(final String[] args) {
		
		SpringApplication.run(BatchConfiguration.class);
		
	}
	
}

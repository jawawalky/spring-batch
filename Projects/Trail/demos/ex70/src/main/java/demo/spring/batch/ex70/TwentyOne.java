/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex70;

import static org.springframework.batch.repeat.RepeatStatus.CONTINUABLE;
import static org.springframework.batch.repeat.RepeatStatus.FINISHED;

import org.springframework.batch.repeat.RepeatCallback;
import org.springframework.batch.repeat.RepeatContext;
import org.springframework.batch.repeat.RepeatStatus;

import demo.util.Demo;
import demo.util.Randomizer;

/**
 * The iterated task.
 * 
 * @author Franz Tost
 *
 */
public class TwentyOne implements RepeatCallback {

	// methods
	// ........................................................................

	@Override public RepeatStatus doInIteration(
		final RepeatContext context
	) throws Exception {
		
		Integer number = (Integer) context.getAttribute("number");
		
		if ((number == null) || (number > 21)) {
			
			number = 0;
			
		} // if
		
		number = number.intValue() + Randomizer.nextInt(13) + 1;
		context.setAttribute("number", number);
		
		Demo.log("%d. Number: %d", context.getStartedCount(), number.intValue());
		
		return (number.intValue() == 21) ? FINISHED : CONTINUABLE;
		
	}
	
}

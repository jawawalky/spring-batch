/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex61;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * The batch configuration.
 * 
 * @author Franz Tost
 *
 */
@Configuration
@EnableBatchProcessing
@EnableAutoConfiguration
public class BatchConfiguration {

	// fields
	// ........................................................................

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	// constructors
	// ........................................................................

	// methods
	// ........................................................................

	public DemoTasklet taskletA() { return new DemoTasklet("A", 2000); }
	public DemoTasklet taskletB() { return new DemoTasklet("B", 1000); }
	public DemoTasklet taskletC() { return new DemoTasklet("C", 2000); }
	public DemoTasklet taskletD() { return new DemoTasklet("D", 4000); }
	public DemoTasklet taskletE() { return new DemoTasklet("E", 2000); }
	
	public Step stepA() { return createStep("stepA", taskletA()); }
	public Step stepB() { return createStep("stepB", taskletB()); }
	public Step stepC() { return createStep("stepC", taskletC()); }
	public Step stepD() { return createStep("stepD", taskletD()); }
	public Step stepE() { return createStep("stepE", taskletE()); }
		
	private Step createStep(final String name, final DemoTasklet tasklet) {
		
		return this.stepBuilderFactory.get(name).tasklet(tasklet).build();
		
	}

	public Flow splitFlow() {
		
		return
			new FlowBuilder<Flow>("splitFlow")
				.split(executor())
				.add(flow1(), flow2())
				.build();
		
	}

	public Flow flow1() {
		
		return
			new FlowBuilder<Flow>("flow1")
				.start(stepB())
				.next(stepC())
				.build();
		
	}

	public Flow flow2() {
		
		return
			new FlowBuilder<Flow>("flow2")
				.start(stepD())
				.build();
		
	}

	@Bean public TaskExecutor executor() {
		
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(5);
		executor.setMaxPoolSize(5);
		executor.setBeanName("executor");
		return executor;
		
	}

	@Bean public Job job() throws Exception {
		
		return
			this.jobBuilderFactory
				.get("job")
				.incrementer(new RunIdIncrementer())
				.flow(stepA())
				.next(splitFlow())
				.next(stepE())
				.end()
				.build();
		
	}
	
}

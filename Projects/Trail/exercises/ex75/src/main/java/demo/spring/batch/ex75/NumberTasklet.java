/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex75;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * A tasklet, which retrieves random numbers from
 * the {@link RandomNumberService}.
 * 
 * @author Franz Tost
 *
 */
public class NumberTasklet implements Tasklet {
	
	// fields
	// ........................................................................
	
	@Autowired private RandomNumberService service;

	// methods
	// ........................................................................

	@Override
	public RepeatStatus execute(
		final StepContribution contribution,
		final ChunkContext     chunkContext
	) throws Exception {
		
		// TODO
		//
		//  o Create a map, whose key are of type 'Class<? extends Throwable>'
		//    and whose values are 'Boolean' objects. Call it
		//    'retryableExceptions'.
		//
		//  o Populate the map with the exceptions, on which a retry should be
		//    performed. If a retry should happen on all kind of exceptions,
		//    then add the key-pair (Exception.class, true).
		//
		//  o Create a 'RetryTemplate' object.
		//
		//  o Set the retry policy to at most 5 tries.
		//
		//  o Implement the 'RetryCallback' that calls the random number
		//    service and pass it to the 'RetryTemplate' object.
		//
		//    Hint: In this case, the return type of the 'RetryTemplate' could
		//          be the 'RepeatStatus' and the Exception type could be
		//          'Exception'.
		//
		
		return null;
		
	}

}

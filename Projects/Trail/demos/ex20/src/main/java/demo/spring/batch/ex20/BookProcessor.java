/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex20;

import org.springframework.batch.item.ItemProcessor;

import demo.util.Demo;

/**
 * A simple item processor, which just logs the processed item.
 * 
 * @author Franz Tost
 *
 */
public class BookProcessor implements ItemProcessor<Book, Book> {
	
	// methods
	// ........................................................................

	@Override
	public Book process(final Book book) throws Exception {
		
		Demo.log("> %s", book);
		return book;
		
	}

}

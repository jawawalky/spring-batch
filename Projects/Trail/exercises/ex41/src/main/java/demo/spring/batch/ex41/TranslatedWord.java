/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex41;

/**
 * A JavaBean modeling a translation.
 * 
 * @author Franz Tost
 *
 */
public class TranslatedWord {
	
	// fields
	// ........................................................................
	
	private int    id;
	
	private int    wordId;
	
	private String text;
	
	// constructors
	// ........................................................................
	
	public TranslatedWord() {
		
		super();

	}

	public TranslatedWord(int id, int wordId, String text) {
		
		super();

		this.id     = id;
		this.wordId = wordId;
		this.text   = text;
		
	}

	// methods
	// ........................................................................
	
	public int    getId()               { return this.id;       }
	public void   setId(int id)         { this.id = id;         }

	public int    getWordId()           { return this.wordId;   }
	public void   setWordId(int wordId) { this.wordId = wordId; }

	public String getText()             { return this.text;     }
	public void   setText(String word)  { this.text = word;     }
	
	public String toString() {

		return "Translation[id='" +
		    this.getId()          +
		    "wordId='"            +
		    this.getWordId()      +
		    "', text='"           +
		    this.getText()        +
		    "']";

	}

}

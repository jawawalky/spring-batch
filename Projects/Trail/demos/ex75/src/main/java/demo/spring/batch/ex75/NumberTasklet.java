/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex75;

import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

import demo.util.Demo;

/**
 * A tasklet, which retrieves random numbers from
 * the {@link RandomNumberService}.
 * 
 * @author Franz Tost
 *
 */
public class NumberTasklet implements Tasklet {
	
	// fields
	// ........................................................................
	
	@Autowired private RandomNumberService service;

	// methods
	// ........................................................................

	@Override
	public RepeatStatus execute(
		final StepContribution contribution,
		final ChunkContext     chunkContext
	) throws Exception {
		
		Map<Class<? extends Throwable>, Boolean> retryableExceptions = new HashMap<>();
		retryableExceptions.put(Exception.class, true);
		
		RetryTemplate retry = new RetryTemplate();
		retry.setRetryPolicy(new SimpleRetryPolicy(2, retryableExceptions));
		return retry.execute(
			new RetryCallback<RepeatStatus, Exception>() {
				@Override public RepeatStatus doWithRetry(
					final RetryContext context
				) throws Exception {
					
					Demo.log("Generated number: %d", service.generate());
					return RepeatStatus.CONTINUABLE;
					
				}
			}
		);
		
	}

}

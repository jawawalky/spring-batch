/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex80;

/**
 * A bean, which contains the scheduled jobs.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Mark this class as a component.
 */
public class Tasks {
	
	// fields
	// ........................................................................

	// TODO
	//
	//  o Auto-wire the job launcher and the job.
	//
	
	// methods
	// ........................................................................

	// TODO
	//
	//  o Schedule the job every 5 seconds.
	//
	//    Hint: Use the annotation '@Scheduled' and the cron pattern
	//          '*/5 * * * * *'.
	//
	public void runJob() {
		
		// TODO
		//
		//  o Create a 'JobParameters' object, where you pass in a time-stamp.
		//    That guarantees that a new job instance is created for each run.
		//
		//  o Launch the job in the standard way.
		//
		
	}
	
}

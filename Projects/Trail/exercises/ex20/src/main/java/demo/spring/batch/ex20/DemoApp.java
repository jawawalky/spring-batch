/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex20;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.util.Demo;

/**
 * This demo, shows you how you can use {@code JobParameters}.
 * 
 * @author Franz Tost
 *
 */
public class DemoApp {

	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	private void runDemo() {
		
		Demo.log("Running demo ...");

		ApplicationContext context = 
			new ClassPathXmlApplicationContext("job.xml");
		
		JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
		Job         job         = (Job)         context.getBean("job");

		try {

			// TODO
			//
			//  o Create a job parameter builder.
			//
			//    Hint: 'org.springframework.batch.core.JobParametersBuilder'
			//
			//  o Add two parameters for the input file and the output file
			//    called
			//
			//      o 'inputFile' and
			//      o 'outputFile'
			//
			//    and set their values.
			//
			//  o Use the builder to create a 'JobParameters' object.
			//
			//  o Pass the parameter object to the job launcher.
			//
			
			JobExecution execution = jobLauncher.run(job, null);
			Demo.log("Exit Status : " + execution.getStatus());

		} // try
		catch (Exception e) {
			
			Demo.log(e);
			
		} // catch

		Demo.log("Finished.");

	}

	/**
	 * Runs the demo.
	 * 
	 * @param args
	 *        no arguments needed.
	 */
	public static void main(final String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}

/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2006 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex90;

/**
 * A JavaBean modeling a word in English.
 * 
 * @author Franz Tost
 *
 */
public class Word {
	
	// fields
	// ........................................................................
	
	private int    id;
	
	private String text;
	
	// constructors
	// ........................................................................
	
	public Word() {
		
		super();

	}

	public Word(int id, String text) {
		
		super();

		this.id   = id;
		this.text = text;
		
	}

	// methods
	// ........................................................................
	
	public int    getId()              { return this.id;   }
	public void   setId(int id)        { this.id = id;     }

	public String getText()            { return this.text; }
	public void   setText(String word) { this.text = word; }
	
	public String toString() {

		return "Word[id='" + this.getId() + "', text='" + this.getText() + "']";

	}

}

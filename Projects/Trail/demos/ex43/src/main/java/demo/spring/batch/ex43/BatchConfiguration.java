/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex43;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.xml.builder.StaxEventItemWriterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

/**
 * The batch configuration.
 * 
 * @author Franz Tost
 *
 */
@Configuration
public class BatchConfiguration {

	// fields
	// ........................................................................

	@Autowired private JobBuilderFactory jobBuilderFactory;

	@Autowired private StepBuilderFactory stepBuilderFactory;

	// methods
	// ........................................................................

	@Bean public Step csv2xml(
		@Autowired ItemReader<Airport>             itemReader,
		@Autowired ItemProcessor<Airport, Airport> itemProcessor,
		@Autowired ItemWriter<Airport>             itemWriter
	) {
		
		return
			this.stepBuilderFactory
				.get("converter")
				.<Airport, Airport>chunk(5)
					.reader(itemReader)
					.processor(itemProcessor)
					.writer(itemWriter)
				.build();
		
	}
	
	@Bean public ItemReader<Airport> itemReader() {
		
		DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
		tokenizer.setNames("code", "name", "city");
		
		FieldSetMapper<Airport> fieldSetMapper = new AirportFieldSetMapper();
		
		DefaultLineMapper<Airport> lineMapper = new DefaultLineMapper<Airport>();
		lineMapper.setLineTokenizer(tokenizer);
		lineMapper.setFieldSetMapper(fieldSetMapper);
		
		return
			new FlatFileItemReaderBuilder<Airport>()
				.name("itemReader")
				.encoding("utf-8")
				.lineMapper(lineMapper)
				.resource(new FileSystemResource("data/input.csv"))
				.build();
		
	}
	
	@Bean public ItemProcessor<Airport, Airport> itemProcessor() {
		
		return v -> v;
		
	}

	@Bean public ItemWriter<Airport> itemWriter() {
		
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setClassesToBeBound(Airport.class);
		
		return
			new StaxEventItemWriterBuilder<Airport>()
				.name("itemWriter")
				.encoding("utf-8")
				.rootTagName("airports")
				.marshaller(marshaller)
				.resource(new FileSystemResource("data/output.xml"))
				.build();
		
	}

	@Bean public Job job(
		@Qualifier("csv2xml") final Step converter
	) throws Exception {
		
		return
			this.jobBuilderFactory
				.get("job")
				.incrementer(new RunIdIncrementer())
				.start(converter)
				.build();
		
	}
	
}

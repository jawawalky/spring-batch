Flat Files
==========


A) Reading Items from a Flat File
=================================

  'BatchConfiguration':
  ---------------------
  
  'itemReader()':
  ---------------
  
  1) Use the 'FlatFileItemReaderBuilder' to create an item reader,
     which can read the numbers of the flat file 'data/input.txt'.
     
  2) Create the item reader with the following settings
  
       o name:           'itemReader'
       o encoding:       'utf-8'
       
  3) Set also the appropriate number of lines to be skipped, due to
     the comment at the beginning of the file 'data/input.txt'.
     
  4) Use a 'org.springframework.batch.item.file.LineMapper<T>'
     to convert the text input into 'Integer' values.
     
  5) Set the input resource.
  
     Hint: Use 'org.springframework.core.io.FileSystemResource'.
	

B) Writing Items to a Flat File
===============================

  'BatchConfiguration':
  ---------------------
  
  'itemWriter()':
  ---------------
  
  1) Use the 'FlatFileItemWriterBuilder' to create an item writer,
     which can write the resulting numbers of the flat file
     'data/output.txt'.
     
  2) Create the item writer with the following settings
     
       o name:           'itemWriter'
       o encoding:       'utf-8'
       
  3) Use a 'org.springframework.batch.item.file.transform.LineAggregator<T>'
     to convert the'Integer' values into text output.
     
  4) Set the output resource.
  
     Hint: Use 'org.springframework.core.io.FileSystemResource'.


C) Creating a Chunked Step
==========================

  'BatchConfiguration':
  ---------------------
  
  'calculateSquares(...)':
  ------------------------
  
  1) Use the 'StepBuilderFactory' to create a chunk-based step, which
  
       o reads numbers from the file 'data/input.txt',
       o calculates their square and
       o writes them to the file 'data/output.txt'.
       
  2) Create the step with the following settings
  
       o chunk-size:     '5'
       o step name:      'calculateSquares'
       
  3) Use the appropriate method parameters as
  
       o item reader,
       o item processor and
       o item writer.

/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex90;

import javax.sql.DataSource;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * A unit test for the batch job.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Run the test with the 'SpringJUnit4ClassRunner'.
 *  
 *  o Configure the context, using the annotation '@ContextConfiguration'.
 */
public class JobTest {
	
	// constants
	// ........................................................................

    private static final String EXPECTED_FILE = "output/expectedWords.csv";
    
    private static final String OUTPUT_FILE   = "output/words.csv";

	// fields
	// ........................................................................

    // TODO
    //
    //  o Autowire the 'JobLauncherTestUtils'.
    //

    private JdbcTemplate jdbcTemplate;

	// methods
	// ........................................................................

    @Autowired
    public void setDataSource(final DataSource dataSource) {
    	
    	this.jdbcTemplate = new JdbcTemplate(dataSource);
    	
    }

	// tests
	// ........................................................................

    @Test public void testJob() throws Exception {

    	// TODO
    	//
    	//  o Drop an existing table 'WORD'.
    	//
    	//  o Create a new 'WORD' table.
    	//
    	//  o Insert values for 'rain', 'sun', 'wind' and 'snow'.
    	//
    	//  Hint: Use the 'JdbcTemplate' and the following SQL commands
    	//        
    	//          o 'drop table WORD'
    	//          o 'create table WORD (id integer, text varchar(30))'
    	//          o 'insert into WORD values (1, 'rain')'
    	//          o 'insert into WORD values (2, 'sun')'
    	//          o 'insert into WORD values (3, 'wind')'
    	//          o 'insert into WORD values (4, 'snow')'
    	//
    	//  o Use the 'JobLauncherTestUtils' instance to launch the job.
    	//
    	//  o Check, that the job terminates with exit code 'COMPLETED'.
    	//
    	//  o Use 'AssertFile.assertFileEquals' to check, if the created file
    	//    'words.csv' is equal to the file 'expectedWords.csv'.
    	//
        
    }

}

/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex90;

import static org.springframework.batch.test.AssertFile.assertFileEquals;

import javax.sql.DataSource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * A unit test for the batch job.
 * 
 * @author Franz Tost
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/job.xml", "/test.xml" })
public class JobTest {
	
	// constants
	// ........................................................................

    private static final String EXPECTED_FILE = "output/expectedWords.csv";
    
    private static final String OUTPUT_FILE   = "output/words.csv";

	// fields
	// ........................................................................

    @Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;

    private JdbcTemplate jdbcTemplate;

	// methods
	// ........................................................................

    @Autowired
    public void setDataSource(final DataSource dataSource) {
    	
    	this.jdbcTemplate = new JdbcTemplate(dataSource);
    	
    }

	// tests
	// ........................................................................

    @Test public void testJob() throws Exception {
    	
    	try { this.jdbcTemplate.update("drop table WORD"); } catch (Exception e) {}
    	this.jdbcTemplate.update("create table WORD (id integer, text varchar(30))");
    	
    	this.jdbcTemplate.update("insert into WORD values (1, 'rain')");
    	this.jdbcTemplate.update("insert into WORD values (2, 'sun')");
    	this.jdbcTemplate.update("insert into WORD values (3, 'wind')");
    	this.jdbcTemplate.update("insert into WORD values (4, 'snow')");
    	
        JobExecution jobExecution = this.jobLauncherTestUtils.launchJob();
        Assert.assertEquals("COMPLETED", jobExecution.getExitStatus().getExitCode());
        
        assertFileEquals(
        	new FileSystemResource(EXPECTED_FILE),
            new FileSystemResource(OUTPUT_FILE)
        );
        
    }

}

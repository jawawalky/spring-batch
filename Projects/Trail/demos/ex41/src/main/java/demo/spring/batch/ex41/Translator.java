/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex41;

import org.springframework.batch.item.ItemProcessor;

import demo.util.Demo;

/**
 * A simple item processor, which calculates the square of a number.
 * 
 * @author Franz Tost
 *
 */
public class Translator implements ItemProcessor<Word, TranslatedWord> {
	
	// fields
	// ........................................................................
	
	private Dictionary dictionary;
	
	private int count;
	
	// constructors
	// ........................................................................
	
	public Translator(final Dictionary dictionary) {
		
		super();
		
		this.dictionary = dictionary;
		
	}

	// methods
	// ........................................................................

	@Override
	public TranslatedWord process(final Word word) throws Exception {
		
		this.count++;
		
		Demo.log("Processing %s ...", word.getText());
		return
			new TranslatedWord(
				this.count,
				word.getId(),
				dictionary.getTranslation(word.getText())
			);
		
	}

}

/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex53;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

import demo.util.Demo;

/**
 * The batch configuration.
 * 
 * @author Franz Tost
 *
 */
@Configuration
public class BatchConfiguration {

	// fields
	// ........................................................................

	@Autowired private JobBuilderFactory jobBuilderFactory;

	@Autowired private StepBuilderFactory stepBuilderFactory;

	// methods
	// ........................................................................

	@Bean public Step transferValues(
		@Autowired ItemReader<String>            itemReader,
		@Autowired ItemProcessor<String, String> itemProcessor,
		@Autowired ItemWriter<String>            itemWriter
	) {
		
		return
			this.stepBuilderFactory
				.get("transferValues")
				.<String, String>chunk(5)
				.reader(itemReader)
				.processor(itemProcessor)
				.writer(itemWriter)
				.faultTolerant()
				.skipLimit(3)
				.skip(CopyException.class)
				.build();
		
	}
	
	@Bean public ItemReader<String> itemReader() {
		
		return
			new FlatFileItemReaderBuilder<String>()
				.name("itemReader")
				.encoding("utf-8")
				.lineMapper((l, i) -> { Demo.log("@:" + i); return l; })
				.resource(new FileSystemResource("data/input.txt"))
				.build();
		
	}
	
	@Bean public ItemProcessor<String, String> itemProcessor() {
		
		return new Copy();
		
	}

	@Bean public ItemWriter<String> itemWriter() {
		
		return
			new FlatFileItemWriterBuilder<String>()
				.name("itemWriter")
				.encoding("utf-8")
				.lineAggregator(v -> v)
				.resource(new FileSystemResource("data/output.txt"))
				.build();
		
	}

	@Bean public Job job(
		@Qualifier("transferValues") final Step transferValues
	) throws Exception {
		
		return
			this.jobBuilderFactory
				.get("job")
				.start(transferValues)
				.build();
		
	}
	
}

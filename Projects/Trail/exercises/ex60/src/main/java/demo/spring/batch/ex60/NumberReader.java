/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex60;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import demo.util.Demo;

/**
 * An item reader, which returns sequential numbers.
 * 
 * @author Franz Tost
 *
 */
public class NumberReader implements ItemReader<Integer> {
	
	// fields
	// ........................................................................
	
	private int count;

	// methods
	// ........................................................................
	
	@Override
	public synchronized Integer read()
		throws
			Exception,
			UnexpectedInputException,
			ParseException,
			NonTransientResourceException
	{
		
		this.count++;
		Demo.log("R [%s] > %d", Thread.currentThread().getName(), this.count);
		
		if (this.count < 100) {
			
			return this.count;
			
		} // if
		
		return null;
		
	}

}

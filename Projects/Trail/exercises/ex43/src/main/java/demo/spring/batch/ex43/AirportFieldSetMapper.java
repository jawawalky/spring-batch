/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex43;

/**
 * A {@code FieldSet} mapper for {@code Airport} objects.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Let this class be a
 *    'org.springframework.batch.item.file.mapping.FieldSetMapper',
 *    which produces 'Airport' objects from the values
 *    
 *      o 'code'
 *      o 'name'
 *      o 'city'
 *      
 *    provided by the 'FieldSet'.
 */
public class AirportFieldSetMapper {

	// methods
	// ........................................................................

}

/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex75;

import java.util.Random;

import demo.util.Demo;

/**
 * A class, which simulates a random number generation service. The generation
 * of a random number may fail. We will use that to demonstrate how
 * a {@code RetryTemplate} can be used.
 * 
 * @author Franz Tost
 *
 */
public class RandomNumberService {
	
	// fields
	// ........................................................................
	
	private Random random = new Random();

	// methods
	// ........................................................................

	public Integer generate() throws Exception {
		
		if (Demo.nextInt(2) == 0) {
			
			Demo.log("Random number generation failed!");
			throw new Exception();
			
		} // if
		
		return this.random.nextInt(10);

	}

}

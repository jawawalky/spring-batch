/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex06;

/**
 * A job execution listener, which logs before the job is started and
 * after the job has terminated.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Derive the class from the interface
 *    'org.springframework.batch.core.JobExecutionListener'.
 *
 */
public class JobExecutionLogger {
	
	// methods
	// ........................................................................
	
	// TODO
	//
	// o Implement the method 'beforeJob(JobExecution)'.
	//
	// o Write a message to the log, which contains the ID of the job and
	//   the time, when the job was created.
	//
	
	
	// TODO
	//
	// o Implement the method 'afterJob(JobExecution)'.
	//
	// o Check the execution status of the job and write some according message
	//   on the console.
	//

}

/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex15;

import javax.sql.DataSource;

import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.explore.support.JobExplorerFactoryBean;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import demo.util.Demo;

/**
 * The batch configuration.
 * 
 * @author Franz Tost
 *
 */
@Configuration
public class ControllerConfiguration {

	// fields
	// ........................................................................
	
	@Autowired private DataSource    dataSource;
	
	@Autowired private JobRepository jobRepository;
	
	@Autowired private JobRegistry   jobRegistry;
	
	@Autowired private JobLauncher   jobLauncher;

	// constructors
	// ........................................................................

	// methods
	// ........................................................................

	@Bean public JobExplorerFactoryBean jobExplorerFactory() {
		
		JobExplorerFactoryBean jobExplorerFactory = new JobExplorerFactoryBean();
		jobExplorerFactory.setDataSource(this.dataSource);
		return jobExplorerFactory;
		
	}
	
	@Bean public JobOperator jobOperator() {
		
		try {
			
			SimpleJobOperator jobOperator = new SimpleJobOperator();
			jobOperator.setJobExplorer(this.jobExplorerFactory().getObject());
			jobOperator.setJobRepository(this.jobRepository);
			jobOperator.setJobRegistry(this.jobRegistry);
			jobOperator.setJobLauncher(this.jobLauncher);
			return jobOperator;
			
		} // try
		catch (Exception e) {
			
			Demo.terminate(e, 1);
			return null;
			
		} // catch
			
	}
	
}

/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex40;

/**
 * An item reader, which reads the numbers from the file
 * {@code input/numbers.txt}.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Let the class implement the interface
 *    'org.springframework.batch.item.ItemReader'.
 */
public class NumberReader {
	
	// fields
	// ........................................................................
	
	// methods
	// ........................................................................
	
	// TODO
	//
	//  o Implement the method 'read()'.
	//
	//  o Read the file 'input/numbers.txt' line by line.
	//
	//  o Convert the each line value to an 'Integer' value and return it.
	//

}

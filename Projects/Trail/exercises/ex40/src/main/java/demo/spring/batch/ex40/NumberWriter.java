/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex40;

/**
 * A writer, which writes the calculated squares to the file
 * {@code output/squares.txt}.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Let the class implement the interface
 *    'org.springframework.batch.item.ItemWriter'.
 */
public class NumberWriter {
	
	// TODO
	//
	//  o Implement the method 'write(List<? extends Integer>)'.
	//
	//  o Write the values to the output file 'output/squares.txt'.
	//
		
}

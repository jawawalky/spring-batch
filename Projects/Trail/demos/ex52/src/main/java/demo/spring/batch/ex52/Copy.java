/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex52;

import org.springframework.batch.item.ItemProcessor;

import demo.util.Demo;

/**
 * A simple item processor, which copies the value.
 * 
 * @author Franz Tost
 *
 */
public class Copy implements ItemProcessor<String, String> {
	
	// methods
	// ........................................................................

	@Override public String process(final String value) throws Exception {
		
		Demo.log("Processing %s ...", value);
		
		if ("fail".equals(value)) {
			
			throw new CopyException();
			
		} // if
		
		return value;
		
	}

}

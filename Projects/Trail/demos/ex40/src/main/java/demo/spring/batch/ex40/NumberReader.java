/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex40;

import java.io.BufferedReader;
import java.io.FileReader;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import demo.util.Demo;

/**
 * An item reader, which reads the numbers from the file
 * {@code input/numbers.txt}.
 * 
 * @author Franz Tost
 *
 */
public class NumberReader implements ItemReader<Integer> {
	
	// fields
	// ........................................................................
	
	private BufferedReader in;

	// methods
	// ........................................................................
	
	@Override
	public Integer read()
		throws
			Exception,
			UnexpectedInputException,
			ParseException,
			NonTransientResourceException
	{
		
		if (this.in == null) {
			
			this.in = new BufferedReader(new FileReader("input/numbers.txt"));
			
		} // if
		
		String line = this.in.readLine();
		
		if (line == null) {
			
			this.in.close();
			this.in = null;
			return null;
			
		} // if
		
		Demo.log("Read %s from input file.", line);
		return Integer.parseInt(line);
		
	}

}

/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex41;

/**
 * A simple item processor, which calculates the square of a number.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Let the class implement the interface
 *    'org.springframework.batch.item.ItemProcessor'.
 */
public class Translator {
	
	// fields
	// ........................................................................
	
	private Dictionary dictionary;
	
	private int count;
	
	// constructors
	// ........................................................................
	
	public Translator(final Dictionary dictionary) {
		
		super();
		
		this.dictionary = dictionary;
		
	}

	// methods
	// ........................................................................
	
	// TODO
	//
	//  o Implement the 'process(Word)' method.
	//
	//  o It should take a 'Word' and translate it.
	//
	//  o The translated word should be returned as a 'TranslatedWord'
	//    instance.
	//
	//  Note:
	//
	//    Don't forget to increment the ID count.
	//
		
}

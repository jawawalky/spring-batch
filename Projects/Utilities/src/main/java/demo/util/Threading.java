/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2012 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.util;

import java.util.Random;

/**
 * A utility class for demo using multi-threading.
 */
public final class Threading {

	// constants
	// ........................................................................

	/**
	 * A random number generator.
	 */
	private static final Random RANDOM = new Random();

	// fields
	// ........................................................................

	// constructors
	// ........................................................................

	/**
	 * A private constructor.
	 */
	private Threading() {}

	// methods
	// ........................................................................
	
	/**
	 * Writes a log messages to the output stream. We add information
	 * about the time, when this method was executed and which thread
	 * called it.
	 * 
	 * @param message
	 *        the message to be printed.
	 *        
	 * @param args
	 *        the template arguments.
	 */
	public static void log(
		final String    message,
		final Object... args
	) {
		
		String msg =
			String.format(
				"[%s] [%-16s] %s",
				Demo.localTime().toString(),
				Thread.currentThread().getName(),
				message
			);
		
		Demo.log(msg, args);

	}

	/**
	 * Lets the current thread sleep for the specified time.
	 * 
	 * @param millis the time in milliseconds.
	 */
	public static void sleep(final long millis) {
		
		try {
			
			Thread.sleep(millis);
			
		} // try
		catch (InterruptedException e) {
			
			// Do nothing.
			
		} // catch
		
	}

	/**
	 * Lets the current thread sleep for some random time between the
	 * minimum and the maximum sleep time.
	 * 
	 * @param minTime the minimum sleep time in milliseconds.
	 * @param minTime the maximum sleep time in milliseconds.
	 */
	public static void sleep(final long minTime, final long maxTime) {
		
		if ((minTime == 0L) && (maxTime == 0L))		return;
		
		sleep(minTime + RANDOM.nextInt((int) (maxTime - minTime)));
		
	}
	
	/**
	 * Returns a random <code>int</code> value in the interval [0, n).
	 * 
	 * @param n the upper limit of the interval.
	 * 
	 * @return The random value.
	 */
	public static int nextInt(final int n) {
		
		return RANDOM.nextInt(n);
		
	}

}

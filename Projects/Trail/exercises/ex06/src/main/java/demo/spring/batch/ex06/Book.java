/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex06;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A domain class for books.
 * 
 * @author Franz Tost
 *
 */
@XmlRootElement
public class Book {

	// fields
	// ........................................................................
	
	private String isbn;
	
	private String author;
	
	private String title;
	
	private double price;

	// constructors
	// ........................................................................

	// methods
	// ........................................................................

	@XmlAttribute
	public String getIsbn()              { return this.isbn;     }
	public void setIsbn(String isbn)     { this.isbn = isbn;     }

	@XmlElement
	public String getAuthor()            { return author;        }
	public void setAuthor(String author) { this.author = author; }

	@XmlElement
	public String getTitle()             { return title;         }
	public void setTitle(String title)   { this.title = title;   }

	@XmlElement
	public double getPrice()             { return price;         }
	public void setPrice(double price)   { this.price = price;   }
	
	@Override
	public String toString() {
		
		return
			"[" + this.getIsbn() + "]: " +
			this.getTitle() + " by " + this.getAuthor() +
			" (" + this.getPrice() + ")";
		
	}

}

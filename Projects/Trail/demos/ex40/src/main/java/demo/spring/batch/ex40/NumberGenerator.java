/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex40;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Random;
import java.util.stream.IntStream;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import demo.util.Demo;

/**
 * A tasklet, which generates 10 random numbers between 0 and 10. The numbers
 * are written to the file {@code input/numbers.txt}.
 * 
 * @author Franz Tost
 *
 */
public class NumberGenerator implements Tasklet {
	
	// fields
	// ........................................................................
	
	private Random random = new Random();

	// methods
	// ........................................................................

	@Override
	public RepeatStatus execute(
		final StepContribution contribution,
		final ChunkContext     chunkContext
	) throws Exception {

		Demo.log("Generating numbers ...");
		
		try (PrintWriter out = new PrintWriter(new FileWriter("input/numbers.txt"))) {
			
			IntStream
				.generate(() -> this.random.nextInt(10))
				.limit(10)
				.forEach(n -> out.println(n));
			
		} // try
		
		return RepeatStatus.FINISHED;
		
	}

}

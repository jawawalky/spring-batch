/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex42;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The batch configuration.
 * 
 * @author Franz Tost
 *
 */
@Configuration
public class BatchConfiguration {

	// fields
	// ........................................................................

	@Autowired private JobBuilderFactory jobBuilderFactory;

	@Autowired private StepBuilderFactory stepBuilderFactory;

	// methods
	// ........................................................................

	@Bean public Step calculateSquares(
		@Autowired ItemReader<Integer>             itemReader,
		@Autowired ItemProcessor<Integer, Integer> itemProcessor,
		@Autowired ItemWriter<Integer>             itemWriter
	) {
		
		// TODO
		//
		//  o Use the 'StepBuilderFactory' to create a chunk-based step,
		//    which
		//
		//      o reads numbers from the file 'data/input.txt',
		//      o calculates their square and
		//      o writes them to the file 'data/output.txt'.
		//
		//  o Create the step with the following settings
		//
		//      o chunk-size:     '5'
		//      o step name:      'calculateSquares'
		//
		//  o Use the appropriate method parameters as
		//
		//      o item reader,
		//      o item processor and
		//      o item writer.
		//
		return null;
		
	}
	
	@Bean public ItemReader<Integer> itemReader() {
		
		// TODO
		//
		//  o Use the 'FlatFileItemReaderBuilder' to create an item reader,
		//    which can read the numbers of the flat file 'data/input.txt'.
		//
		//  o Create the item reader with the following settings
		//
		//      o name:           'itemReader'
		//      o encoding:       'utf-8'
		//
		//  o Set also the appropriate number of lines to be skipped, due to
		//    the comment at the beginning of the file 'data/input.txt'.
		//
		//  o Use a 'org.springframework.batch.item.file.LineMapper<T>'
		//    to convert the text input into 'Integer' values.
		//
		//  o Set the input resource.
		//
		//    Hint: Use 'org.springframework.core.io.FileSystemResource'.
		//
		return null;
		
	}
	
	@Bean public ItemProcessor<Integer, Integer> itemProcessor() {
		
		return new SquareCalculator();
		
	}

	@Bean public ItemWriter<Integer> itemWriter() {
		
		// TODO
		//
		//  o Use the 'FlatFileItemWriterBuilder' to create an item writer,
		//    which can write the resulting numbers of the flat file
		//    'data/output.txt'.
		//
		//  o Create the item writer with the following settings
		//
		//      o name:           'itemWriter'
		//      o encoding:       'utf-8'
		//
		//  o Use a 'org.springframework.batch.item.file.transform.LineAggregator<T>'
		//    to convert the'Integer' values into text output.
		//
		//  o Set the output resource.
		//
		//    Hint: Use 'org.springframework.core.io.FileSystemResource'.
		//
		return null;
		
	}

	@Bean public Job job(
		@Qualifier("calculateSquares") final Step calculateSquares
	) throws Exception {
		
		return
			this.jobBuilderFactory
				.get("job")
				.incrementer(new RunIdIncrementer())
				.start(calculateSquares)
				.build();
		
	}
	
}

/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex44;

import javax.sql.DataSource;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

/**
 * A configuration for the database connection.
 * 
 * @author Franz Tost
 *
 */
@Configuration
@EnableBatchProcessing
public class DatabaseConfiguration {

    // methods
	// ........................................................................
	
	@Bean public DataSource dataSource() {
		
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("org.apache.derby.jdbc.ClientDriver");
		dataSource.setUrl("jdbc:derby://localhost:1527/db/demo;create=true");
		dataSource.setUsername("app");
		dataSource.setPassword("x");
		
		return dataSource;
		
	}
	
	@Bean public ResourceDatabasePopulator databasePopulator() {
		
		ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
		
		populator.addScript(new ClassPathResource("org/springframework/batch/core/schema-drop-derby.sql"));
		populator.addScript(new ClassPathResource("org/springframework/batch/core/schema-derby.sql"));
		populator.setIgnoreFailedDrops(true);
		
		return populator;
		
	}
	
	@Bean public DataSourceInitializer databaseInitializer() {
		
		DataSourceInitializer initializer = new DataSourceInitializer();
		
		initializer.setDataSource(dataSource());
		initializer.setDatabasePopulator(databasePopulator());
		
		return initializer;
		
	}
	
}

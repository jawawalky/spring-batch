/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex02;

/**
 * A simple item processor, which just logs the processed item.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Let the 'BookProcessor' implement the 'ItemProcessor' interface.
 */
public class BookProcessor {
	
	// methods
	// ........................................................................
	
	// TODO
	//
	//  o Override the 'process(...)' method.
	//
	//  o Print the book on the console.
	//
	//  o Return the book instance.
	//

}

/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex43;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A JavaBean modeling an airport.
 * 
 * @author Franz Tost
 *
 */
@XmlRootElement
public class Airport {

	// fields
	// ........................................................................

	private String code;
	private String name;
	private String city;

	// constructors
	// ........................................................................

	public Airport() { }

	public Airport(String code, String name, String city) {

		super();

		this.code = code;
		this.name = name;
		this.city = city;

	}

	// methods
	// ........................................................................

	@XmlElement
	public String getCode()            { return this.code; }
	public void   setCode(String code) { this.code = code; }

	@XmlElement
	public String getName()            { return this.name; }
	public void   setName(String name) { this.name = name; }

	@XmlElement
	public String getCity()            { return this.city; }
	public void   setCity(String city) { this.city = city; }

	public String toString() {

		return this.getName() + " (" + this.getCode() + ")";

	}

}

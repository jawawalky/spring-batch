/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex43;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The batch configuration.
 * 
 * @author Franz Tost
 *
 */
@Configuration
public class BatchConfiguration {

	// fields
	// ........................................................................

	@Autowired private JobBuilderFactory jobBuilderFactory;

	@Autowired private StepBuilderFactory stepBuilderFactory;

	// methods
	// ........................................................................

	@Bean public Step csv2xml(
		@Autowired ItemReader<Airport>             itemReader,
		@Autowired ItemProcessor<Airport, Airport> itemProcessor,
		@Autowired ItemWriter<Airport>             itemWriter
	) {
		
		return
			this.stepBuilderFactory
				.get("converter")
				.<Airport, Airport>chunk(5)
					.reader(itemReader)
					.processor(itemProcessor)
					.writer(itemWriter)
				.build();
		
	}
	
	@Bean public ItemReader<Airport> itemReader() {
		
		// TODO
		//
		//  o Use a 'DelimitedLineTokenizer' to split the comma-separated
		//    tokens of the input file 'input.csv'.
		//
		//  o The tokens should have the names
		//
		//      o 'code'
		//      o 'name'
		//      o 'city'
		//
		//  o Create a line mapper, using the class
		//    'org.springframework.batch.item.file.mapping.DefaultLineMapper',
		//    which should create 'Airport' objects from each line of
		//    the input file 'data/input.csv'.
		//
		//  o Configure the line mapper with appropriate values for
		//
		//      o the line tokenizer and
		//      o the 'FieldSet' mapper.
		//
		//  o Create a 'FlatFileItemReaderBuilder', which reads the file
		//    'data/input.csv' and uses the previously defined line mapper.
		//
		
		return null;
		
	}
	
	@Bean public ItemProcessor<Airport, Airport> itemProcessor() {
		
		return v -> v;
		
	}

	@Bean public ItemWriter<Airport> itemWriter() {
		
		// TODO
		//
		//  o Create a 'org.springframework.oxm.jaxb.Jaxb2Marshaller'.
		//
		//  o Bind the class 'Airport' to the marshaller.
		//
		//  o Create a 'org.springframework.batch.item.xml.builder.StaxEventItemWriterBuilder',
		//    which uses the previously created marshaller to write
		//    the output file 'data/output.xml'.
		//
		
		return null;
		
	}

	@Bean public Job job(
		@Qualifier("csv2xml") final Step converter
	) throws Exception {
		
		return
			this.jobBuilderFactory
				.get("job")
				.incrementer(new RunIdIncrementer())
				.start(converter)
				.build();
		
	}
	
}

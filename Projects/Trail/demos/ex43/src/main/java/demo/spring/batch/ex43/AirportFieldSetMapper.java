/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2010 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex43;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

/**
 * A {@code FieldSet} mapper for {@code Airport} objects.
 * 
 * @author Franz Tost
 *
 */
public class AirportFieldSetMapper implements FieldSetMapper<Airport> {

	// methods
	// ........................................................................

	@Override public Airport mapFieldSet(
		final FieldSet fieldSet
	) throws BindException {
		
		return
			new Airport(
				fieldSet.readString("code"),
				fieldSet.readString("name"),
				fieldSet.readString("city")
			);
		
	}

}

Guide
=====

A) Building the Eclipse Project
===============================

  1) Open a terminal window.
  
  2) Go to the project folder.
  
  3) Run the command
  
       gradle cleanEclipse eclipse
      
      
B) Building the Project
=======================

  1) Open a terminal window.
  
  2) Go to the project folder.
  
  3) Run the command
  
       gradle clean build
      
      
C) Running the Demo
===================

  1) You can run the demo by simply starting 'DemoApp' inside your Eclipse IDE.

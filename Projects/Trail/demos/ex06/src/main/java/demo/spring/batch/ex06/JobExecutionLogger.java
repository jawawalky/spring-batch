/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex06;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

import demo.util.Demo;

/**
 * A job execution listener, which logs before the job is started and
 * after the job has terminated.
 * 
 * @author Franz Tost
 *
 */
public class JobExecutionLogger implements JobExecutionListener {
	
	// methods
	// ........................................................................

	@Override
	public void beforeJob(final JobExecution jobExecution) {
		
		Demo.log(
			"Job %d created at %s.",
			jobExecution.getJobId(),
			jobExecution.getCreateTime().toString()
		);

	}

	@Override
	public void afterJob(final JobExecution jobExecution) {

		Long        jobId  = jobExecution.getJobId();
		BatchStatus status = jobExecution.getStatus();
		
		switch (status) {
		
		case COMPLETED:
			Demo.log("Job %s completed.", jobId);
			break;
			
		case ABANDONED:
			Demo.log("Job %s abandoned.", jobId);
			break;
			
		case FAILED:
			Demo.log("Job %s failed.", jobId);
			break;			
			
		default:
			Demo.log("Job %d with strange status: %s", jobId, status.name());
			
		} // switch
		
	}

}

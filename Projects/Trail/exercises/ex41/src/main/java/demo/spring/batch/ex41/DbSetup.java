/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex41;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import demo.util.Demo;

/**
 * The tasklet drops existing database tables <i>WORD</i> and
 * <i>TRANSLATED_WORD</i> and creates them as empty tables.
 * 
 * @author Franz Tost
 *
 */
public class DbSetup implements Tasklet {
	
	// fields
	// ........................................................................
	
	private DataSource dataSource;
	
	// constructors
	// ........................................................................
	
	public DbSetup(final DataSource dataSource) {
		
		super();
		
		this.dataSource = dataSource;
		
	}

	// methods
	// ........................................................................

	@Override
	public RepeatStatus execute(
		final StepContribution contribution,
		final ChunkContext     chunkContext
	) throws Exception {
		
		try (
			Connection connection = this.dataSource.getConnection();
			Statement  statement  = connection.createStatement();
		) {
			
			// Dropping existing tables.
			//

			try {
				
				Demo.log("Dropping table TRANSLATED_WORD ...");
				statement.executeUpdate("drop table TRANSLATED_WORD");
				Demo.log("... ok!");
				
			} // try
			catch (SQLException e) {
				
				Demo.log("... not found!");
				
			} // catch
			
			try {
				
				Demo.log("Dropping table WORD ...");
				statement.executeUpdate("drop table WORD");
				Demo.log("... ok!");
				
			} // try
			catch (SQLException e) {
				
				Demo.log("... not found!");
				
			} // catch
			
			
			// Creating new tables.
			//
			
			try {
				
				Demo.log("Creating table WORD ...");
				statement.executeUpdate("create table WORD (id integer, text varchar(30))");
				Demo.log("... ok!");
				
			} // try
			catch (SQLException e) {
				
				Demo.log("... failed!");
				throw e;
				
			} // catch
			
			try {
				
				Demo.log("Creating table TRANSLATED_WORD ...");
				statement.executeUpdate("create table TRANSLATED_WORD (id integer, wordId integer, text varchar(30))");
				Demo.log("... ok!");
				
			} // try
			catch (SQLException e) {
				
				Demo.log("... failed!");
				throw e;
				
			} // catch
			
		} // try

		return RepeatStatus.FINISHED;
		
	}

}

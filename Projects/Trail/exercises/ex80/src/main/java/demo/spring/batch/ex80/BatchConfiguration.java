/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex80;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.repeat.policy.SimpleCompletionPolicy;
import org.springframework.batch.repeat.support.RepeatTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The batch configuration.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Enable scheduling
 *  
 *    Hint: Use according annotation.
 */
@Configuration
@EnableBatchProcessing
@EnableAutoConfiguration
public class BatchConfiguration {

	// fields
	// ........................................................................

	@Autowired private JobBuilderFactory  jobBuilderFactory;

	@Autowired private StepBuilderFactory stepBuilderFactory;

	// constructors
	// ........................................................................

	// methods
	// ........................................................................

	@Bean public NumberReader    reader()    { return new NumberReader();    }
	@Bean public NumberProcessor processor() { return new NumberProcessor(); }
	@Bean public NumberWriter    writer()    { return new NumberWriter();    }
	
	@Bean public RepeatTemplate  repeater() {
		
		RepeatTemplate repeater = new RepeatTemplate();
		repeater.setCompletionPolicy(new SimpleCompletionPolicy(2));
		return repeater;
		
	}

	@Bean public Step step() {
		
		return
			this.stepBuilderFactory
				.get("step")
				.<Integer, Integer>chunk(5)
				.reader(this.reader())
				.processor(this.processor())
				.writer(this.writer())
				.stepOperations(this.repeater()) // <- Limits the number of chunks per execution
				.build();
		
	}

	@Bean public Job job(final Step step) throws Exception {
		
		return
			this.jobBuilderFactory
				.get("job")
				.incrementer(new RunIdIncrementer())
				.start(this.step())
				.build();
		
	}
	
	@Bean public Tasks tasks() { return new Tasks(); }
	
}

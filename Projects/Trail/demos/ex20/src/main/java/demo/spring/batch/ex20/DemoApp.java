/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex20;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.util.Demo;

/**
 * This demo, shows you how you can use {@code JobParameters}.
 * 
 * @author Franz Tost
 *
 */
public class DemoApp {

	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	private void runDemo() {
		
		Demo.log("Running demo ...");

		ApplicationContext context = 
			new ClassPathXmlApplicationContext("job.xml");
		
		JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
		Job         job         = (Job)         context.getBean("job");

		try {

			JobParametersBuilder parametersBuilder = new JobParametersBuilder();
			parametersBuilder.addString("inputFile",  "books.csv");
			parametersBuilder.addString("outputFile", "books.xml");
			
			JobParameters parameters = parametersBuilder.toJobParameters();
			
			JobExecution execution = jobLauncher.run(job, parameters);
			Demo.log("Exit Status : " + execution.getStatus());

		} // try
		catch (Exception e) {
			
			Demo.log(e);
			
		} // catch

		Demo.log("Finished.");

	}

	/**
	 * Runs the demo.
	 * 
	 * @param args
	 *        no arguments needed.
	 */
	public static void main(final String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}

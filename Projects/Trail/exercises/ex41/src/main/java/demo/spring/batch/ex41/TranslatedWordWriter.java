/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex41;

/**
 * A prepared statement setter maps {@link TranslatedWord} objects to
 * database records via a prepared statement.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Let the class implement the interface
 *    'org.springframework.batch.item.database.ItemPreparedStatementSetter'.
 */
public class TranslatedWordWriter {

	// methods
	// ........................................................................
	
	// TODO
	//
	//  o Implement the method
	//    'setValues(TranslatedWord translatedWord, PreparedStatement pstmt)'.
	//
	//  o Map the values of the 'TranslatedWord' to the database record to be
	//    written.
	//
		
}

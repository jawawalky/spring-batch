/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2021 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.derby;

import java.net.InetAddress;

import org.apache.derby.drda.NetworkServerControl;

import demo.util.Demo;
import demo.util.Threading;

/**
 * A simple class starting the <i>Derby Network Server</i> programmatically.
 * 
 * @author Franz Tost
 *
 */
public class DerbyServer {

	// constructors /////

	private DerbyServer() { }
	

	// methods /////
	
	private void run() {
		
		Demo.log("Starting Derby Network Server ...");
		
		System.setProperty("derby.drda.startNetworkServer", "true");
		
		NetworkServerControl server = null;
		
		try {
			
			server =
				new NetworkServerControl(
					InetAddress.getByName("localhost"),
					1527
				);
			
			server.start(null);
			
			boolean running = false;
				
			while (!running) {
				
				Threading.sleep(1000);

				try { server.ping(); running = true; }
				catch (Exception e) { Demo.log(e.getMessage()); }
				
			}
			
			Demo.log("Server is running ...");
			Demo.input("Shut down server with Ctrl+C or with via IDE.");

		} // try
		catch (Exception e) {
			
			Demo.log(e);
			
		} // catch
		finally {
			
			try {
				
				server.shutdown();
				
			} // try
			catch (Exception e) {
				
				Demo.log(e);
				
			} // catch
			
		} // finally
		
		Demo.log("Finished.");

	}

	/**
	 * Runs the server.
	 * 
	 * @param args
	 *        no arguments needed.
	 */
	public static void main(final String[] args) {
		
		new DerbyServer().run();
		
	}
	
}

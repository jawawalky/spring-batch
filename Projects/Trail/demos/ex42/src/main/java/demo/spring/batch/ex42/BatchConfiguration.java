/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex42;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

/**
 * The batch configuration.
 * 
 * @author Franz Tost
 *
 */
@Configuration
public class BatchConfiguration {

	// fields
	// ........................................................................

	@Autowired private JobBuilderFactory jobBuilderFactory;

	@Autowired private StepBuilderFactory stepBuilderFactory;

	// methods
	// ........................................................................

	@Bean public Step calculateSquares(
		@Autowired ItemReader<Integer>             itemReader,
		@Autowired ItemProcessor<Integer, Integer> itemProcessor,
		@Autowired ItemWriter<Integer>             itemWriter
	) {
		
		return
			this.stepBuilderFactory
				.get("calculateSquares")
				.<Integer, Integer>chunk(5)
					.reader(itemReader)
					.processor(itemProcessor)
					.writer(itemWriter)
				.build();
		
	}
	
	@Bean public ItemReader<Integer> itemReader() {
		
		return
			new FlatFileItemReaderBuilder<Integer>()
				.name("itemReader")
				.encoding("utf-8")
				.linesToSkip(2)
				.lineMapper((l, i) -> Integer.parseInt(l))
				.resource(new FileSystemResource("data/input.txt"))
				.build();
		
	}
	
	@Bean public ItemProcessor<Integer, Integer> itemProcessor() {
		
		return new SquareCalculator();
		
	}

	@Bean public ItemWriter<Integer> itemWriter() {
		
		return
			new FlatFileItemWriterBuilder<Integer>()
				.name("itemWriter")
				.encoding("utf-8")
				.lineAggregator(v -> v.toString())
				.resource(new FileSystemResource("data/output.txt"))
				.build();
		
	}

	@Bean public Job job(
		@Qualifier("calculateSquares") final Step calculateSquares
	) throws Exception {
		
		return
			this.jobBuilderFactory
				.get("job")
				.incrementer(new RunIdIncrementer())
				.start(calculateSquares)
				.build();
		
	}
	
}

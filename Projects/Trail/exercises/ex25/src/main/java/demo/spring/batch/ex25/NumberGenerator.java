/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex25;

import java.util.Random;

/**
 * A tasklet, which generates 10 random numbers between 0 and 10. The numbers
 * are written to the file {@code input/numbers.txt}.
 * 
 * @author Franz Tost
 *
 * TODO
 * 
 *  o Let the class implement the interface
 *    'org.springframework.batch.core.step.tasklet.Tasklet'
 */
public class NumberGenerator {
	
	// fields
	// ........................................................................
	
	private Random random = new Random();

	// methods
	// ........................................................................

	// TODO
	//
	//  o Implement the method
	//    'execute(StepContribution, ChunkContext chunkContext)'.
	//
	//  o Write to the file 'input/number.txt' 10 random numbers between
	//    0 and 9.
	//
	//  o Return an appropriate repeat status.
	//

}

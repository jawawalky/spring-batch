/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex41;

/**
 * A row manager maps the values of a result set row to {@link Word} objects.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Let the class implement the interface
 *    'org.springframework.jdbc.core.RowMapper'.
 */
public class WordMapper {

	// methods
	// ........................................................................
	
	// TODO
	//
	//  o Implement the method 'mapRow(ResultSet, int)'.
	//
	//  o Map the read database record to a 'Word' instance.
	//

}

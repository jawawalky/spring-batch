/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex30;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import demo.util.Demo;

/**
 * A tasklet, which may fail.
 * 
 * @author Franz Tost
 *
 */
public class Tasklet1 implements Tasklet {
	
	// methods
	// ........................................................................

	@Override
	public RepeatStatus execute(
		final StepContribution contribution,
		final ChunkContext     chunkContext
	) throws Exception {
		
		Demo.log("Tasklet 1");
		
		// TODO
		//
		//  o Let this tasklet fail with a probability of 50%, i.e. throw
		//    an exception with 50% probability.
		//
		//    Hint: Use 'Demo.nextInt(...)'
		//

		return RepeatStatus.FINISHED;
		
	}

}

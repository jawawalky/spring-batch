/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex75;

import org.springframework.boot.SpringApplication;

/**
 * This is a simple demo, which shows you how you parallelize a step of
 * a batch job.
 * 
 * @author Franz Tost
 *
 */
public class DemoApp {

	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	/**
	 * Runs the demo.
	 * 
	 * @param args
	 *        no arguments needed.
	 */
	public static void main(final String[] args) {
		
		System.exit(
			SpringApplication.exit(
				SpringApplication.run(
					BatchConfiguration.class, args
				)
			)
		);
		
	}
	
}

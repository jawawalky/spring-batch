/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2007 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex41;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.batch.item.database.ItemPreparedStatementSetter;

/**
 * A prepared statement setter maps {@link TranslatedWord} objects to
 * database records via a prepared statement.
 * 
 * @author Franz Tost
 *
 */
public class TranslatedWordWriter
	implements
		ItemPreparedStatementSetter<TranslatedWord>
{

	// methods
	// ........................................................................

	@Override
	public void setValues(
		final TranslatedWord    translatedWord,
		final PreparedStatement pstmt
	) throws SQLException {
		
		pstmt.setInt(1, translatedWord.getId());
		pstmt.setInt(2, translatedWord.getWordId());
		pstmt.setString(3, translatedWord.getText());
		
	}

}

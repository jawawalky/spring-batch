/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex50;

import org.springframework.batch.item.ItemProcessor;

import demo.util.Demo;

/**
 * A simple item processor, which calculates the square of a number.
 * 
 * @author Franz Tost
 *
 */
public class NumberProcessor implements ItemProcessor<Integer, Integer> {
	
	// methods
	// ........................................................................

	@Override
	public Integer process(final Integer number) throws Exception {
		
		Demo.log("Processing %d ...", number);
		
		// TODO
		//
		//  o Simulate a 10% failure probability.
		//
		//    Hint: Use 'Demo.nextInt(int)' and throw an exception,
		//          when the method should fail.
		//
		
		return number * number;
		
	}

}

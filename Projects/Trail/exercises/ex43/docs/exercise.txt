FieldSet
========


A) Implementing a FieldSetMapper
================================

  'AirportFieldSetMapper':
  ------------------------
  
  1) Let this class be a
     'org.springframework.batch.item.file.mapping.FieldSetMapper',
     which produces 'Airport' objects from the values
     
       o 'code'
       o 'name'
       o 'city'
       
     provided by the 'FieldSet'.
     
     
B) Configuring the Item Reader
==============================

  'BatchConfiguration':
  ---------------------
  
  'itemReader()':
  ---------------
  
  1) Use a 'DelimitedLineTokenizer' to split the comma-separated
     tokens of the input file 'input.csv'.
     
  2) The tokens should have the names
  
       o 'code'
       o 'name'
       o 'city'
       
  3) Create a line mapper, using the class
     'org.springframework.batch.item.file.mapping.DefaultLineMapper',
     which should create 'Airport' objects from each line of
     the input file 'data/input.csv'.
     
  4) Configure the line mapper with appropriate values for
     
       o the line tokenizer and
       o the 'FieldSet' mapper.
       
  5) Create a 'FlatFileItemReaderBuilder', which reads the file
     'data/input.csv' and uses the previously defined line mapper.
     
     
C) Configuring the Item Writer
==============================

  'BatchConfiguration':
  ---------------------
  
  'itemWriter()':
  ---------------
  
  1) Create a 'org.springframework.oxm.jaxb.Jaxb2Marshaller'.
  
  2) Bind the class 'Airport' to the marshaller.
  
  3) Create a 'org.springframework.batch.item.xml.builder.StaxEventItemWriterBuilder',
     which uses the previously created marshaller to write
     the output file 'data/output.xml'.

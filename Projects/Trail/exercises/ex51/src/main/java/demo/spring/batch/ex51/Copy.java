/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex51;

import org.springframework.batch.item.ItemProcessor;

/**
 * A simple item processor, which copies the value.
 * 
 * @author Franz Tost
 *
 */
public class Copy implements ItemProcessor<String, String> {
	
	// methods
	// ........................................................................

	@Override public String process(final String value) throws Exception {
		
		// TODO
		//
		//  o Print the value being processed on the console.
		//
		//  o If the processed value is 'fail', then throw a runtime
		//    exception.
		//
		//  o Return the value, which was passed into the method.
		//
		return null;
		
	}

}

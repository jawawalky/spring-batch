/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex51;

import javax.sql.DataSource;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * A configuration for the database connection.
 * 
 * @author Franz Tost
 *
 */
@Configuration
@EnableBatchProcessing
public class DatabaseConfiguration {

    // methods
	// ........................................................................
	
	@Bean public DataSource dataSource() {
		
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("org.apache.derby.jdbc.ClientDriver");
		dataSource.setUrl("jdbc:derby://localhost:1527/db/demo;create=true");
		dataSource.setUsername("app");
		dataSource.setPassword("x");
		
		return dataSource;
		
	}
	
	// INFO
	//
	//  Remember to set up the required database structures for Spring Batch.
	//  Use 'DerbySchemaGenerator' from the 'Spring Batch' project.
	//
	
}

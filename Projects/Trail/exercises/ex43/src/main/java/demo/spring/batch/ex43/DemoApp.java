/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex43;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import demo.util.Demo;

/**
 * This demo shows you, how to use a {@code FieldSet}.
 * 
 * @author Franz Tost
 *
 */
public class DemoApp {

	// constructors
	// ........................................................................

	private DemoApp() { }

	// methods
	// ........................................................................
	
	private void runDemo() {
		
		Demo.log("Running demo ...");

		try (
			AnnotationConfigApplicationContext context =
				new AnnotationConfigApplicationContext(
					DatabaseConfiguration.class,
					BatchConfiguration.class
				);
		) {

			JobLauncher   launcher   = (JobLauncher) context.getBean("jobLauncher");
			Job           job        = (Job)         context.getBean("job");
			JobParameters parameters = new JobParameters();
			
			launcher.run(job, parameters);

			Demo.log("Finished.");

		} // try
		catch (Exception e) {
			
			Demo.log(e);
			
		} // catch

	}

	/**
	 * Runs the demo.
	 * 
	 * @param args
	 *        no arguments needed.
	 */
	public static void main(final String[] args) {
		
		new DemoApp().runDemo();
		
	}
	
}

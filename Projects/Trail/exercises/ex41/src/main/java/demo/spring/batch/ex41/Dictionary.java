/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex41;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * The dictionary contains the translations.
 * 
 * @author Franz Tost
 *
 */
public class Dictionary {
	
	// fields
	// ........................................................................
	
	private Map<String, String> translations = new HashMap<>();
	
	{
		this.translations.put("rain", "Regen");
		this.translations.put("sun",  "Sonne");
		this.translations.put("wind", "Wind");
		this.translations.put("snow", "Schnee");
	}
	
	// methods
	// ........................................................................
	
	public Set<String> getWords() {
		
		return this.translations.keySet();
		
	}
	
	public String getTranslation(final String wordInEnglish) {
		
		return this.translations.get(wordInEnglish);
		
	}

}

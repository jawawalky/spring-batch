/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex15;

import java.util.Set;

import org.springframework.batch.core.launch.JobOperator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import demo.util.Demo;

/**
 * This demo shows you, how you can schedule a job.
 * 
 * @author Franz Tost
 *
 */
public class DemoApp {

	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	/**
	 * Runs the demo.
	 * 
	 * @param args
	 *        no arguments needed.
	 */
	public static void main(final String[] args) {
		
		ApplicationContext context =
			new AnnotationConfigApplicationContext(
				DatabaseConfiguration.class,
				ControllerConfiguration.class
			);
		
		try {
			
			JobOperator jobOperator = (JobOperator) context.getBean("jobOperator");
			Set<Long>   executions  = jobOperator.getRunningExecutions("job");
			jobOperator.stop(executions.iterator().next());
			
		} // try
		catch (Exception e) {
			
			Demo.log(e);
			
		} // catch

	}
	
}

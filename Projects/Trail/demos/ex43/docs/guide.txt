Guide
=====

A) Building the Eclipse Project
===============================

  1) Open a terminal window.
  
  2) Go to the project folder.
  
  3) Run the command
  
       gradle cleanEclipse eclipse
      
      
B) Building the Project
=======================

  1) Open a terminal window.
  
  2) Go to the project folder.
  
  3) Run the command
  
       gradle clean build
      
      
C) Running the Demo
===================

  1) Remove the file 'data/output.txt'.
  
  2) Run the demo by simply starting 'DemoApp' inside your Eclipse IDE.
  
  3) Refresh your project after running the application. You should see
     a newly created file 'data/output.txt' with the squared values of
     the 'data/input.txt' file.

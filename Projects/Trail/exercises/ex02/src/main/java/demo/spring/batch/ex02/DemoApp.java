/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex02;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import demo.util.Demo;

/**
 * This is a simple demo, which shows you how you can configure your beans.
 * 
 * @author Franz Tost
 *
 */
public class DemoApp {

	// constructors
	// ........................................................................

	private DemoApp() {}

	// methods
	// ........................................................................
	
	private void runDemo() {
		
		Demo.log("Running demo ...");

		try (
			ClassPathXmlApplicationContext context = 
				new ClassPathXmlApplicationContext("job.xml");
		) {
			
			// TODO
			//
			//  o Get the 'JobLauncher' instance from the bean context.
			//
			//  o Get the 'Job' instance 'job' from the bean context.
			//
			//  o Use the 'JobLauncher' to run the job.
			//
			//  o Check the status of the job execution.
			//
			
		} // try
		
		Demo.log("Finished.");

	}

	/**
	 * Runs the demo.
	 * 
	 * @param args
	 *        no arguments needed.
	 */
	public static void main(final String[] args) {
		
		DemoApp app = new DemoApp();
		app.runDemo();
		
	}
	
}

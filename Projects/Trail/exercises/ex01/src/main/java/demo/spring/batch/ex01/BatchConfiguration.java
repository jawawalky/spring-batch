/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex01;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;

/**
 * The batch configuration.
 * 
 * @author Franz Tost
 * 
 * TODO
 * 
 *  o Make this class a configuration class by annotating it with
 *    'org.springframework.context.annotation.Configuration'.
 *    
 *  o Enable batch processing with the appropriate annotation.
 *  
 *  o Enable auto configuration with the appropriate annotation.
 *
 */
public class BatchConfiguration {

	// fields
	// ........................................................................
	
	// TODO
	//
	//  When batch processing is enabled, then certain objects will be
	//  provided automatically at the start of the application.
	//
	//  o Autowire a 'JobBuilderFactory', which can be used to construct
	//    the batch job.
	//
	//  o Autowire a 'StepBuilderFactory', which can be used to construct
	//    the steps of a batch job.
	//

	// constructors
	// ........................................................................

	// methods
	// ........................................................................

	// TODO
	//
	//  o Declare this method as a bean method.
	//
	//    Hint: Use an appropriate annotation.
	//
	public Step step1() {
		
		// TODO
		//
		//  o Create a step called 'step1' using the step builder factory.
		//
		//  o Add a tasklet instance by creating an anonymous sub-class of
		//    the class 'org.springframework.batch.core.step.tasklet.Tasklet'.
		//
		//  o Implement the 'execute(...)' method of that tasklet by returning
		//    'null'.
		//
		//  o Build the step instance.
		//
		//    Hint:
		//
		//      The step builder uses a 'fluent' API.
		//
		return null;
		
	}

	// TODO
	//
	//  o Declare this method as a bean method.
	//
	//    Hint: Use an appropriate annotation.
	//
	public Job job(final Step step1) throws Exception {
		
		// TODO
		//
		//  o Create a job called 'job1' using the job builder factory.
		//
		//  o Add an incrementer of type 'RunIdIncrementer'.
		//
		//  o Set the first step as the start step.
		//
		//  o Build the job instance.
		//
		//    Hint:
		//
		//      The job builder uses a 'fluent' API.
		//
		return null;

	}
	
}

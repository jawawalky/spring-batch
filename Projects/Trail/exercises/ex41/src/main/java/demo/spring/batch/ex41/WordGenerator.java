/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex41;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import javax.sql.DataSource;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import demo.util.Demo;

/**
 * A tasklet, which generates 1000 random words and writes them to
 * the table <i>WORD</i>.
 * 
 * @author Franz Tost
 *
 */
public class WordGenerator implements Tasklet {
	
	// fields
	// ........................................................................
	
	private Random       random = new Random();

	private DataSource   dataSource;
	
	private List<String> words;
	
	private int          count;
	
	// constructors
	// ........................................................................
	
	public WordGenerator(
		final DataSource dataSource,
		final Dictionary dictionary
	) {
		
		super();
		
		this.dataSource = dataSource;
		this.words      = new ArrayList<>(dictionary.getWords());
		
	}

	// methods
	// ........................................................................

	@Override
	public RepeatStatus execute(
		final StepContribution contribution,
		final ChunkContext     chunkContext
	) throws Exception {

		Demo.log("Generating words ...");
		
		IntStream
			.generate(() -> this.random.nextInt(this.words.size()))
			.limit(1000)
			.forEach(this::writeWord);
			
		return RepeatStatus.FINISHED;
		
	}
	
	private void writeWord(final int index) {
		
		this.count++;
		String word = this.words.get(index);
		
		try (
			Connection connection = this.dataSource.getConnection();
			Statement  statement  = connection.createStatement();
		) {
			
			statement.executeUpdate("insert into WORD values (" + this.count + ", '" + word + "')");
			
		} // try
		catch (SQLException e) {
			
			e.printStackTrace();
			
		} // catch

	}

}

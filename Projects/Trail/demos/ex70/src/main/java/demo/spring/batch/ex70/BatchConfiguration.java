/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2017 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.batch.ex70;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.repeat.policy.SimpleCompletionPolicy;
import org.springframework.batch.repeat.support.RepeatTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The batch configuration.
 * 
 * @author Franz Tost
 *
 */
@Configuration
@EnableBatchProcessing
@EnableAutoConfiguration
public class BatchConfiguration {

	// fields
	// ........................................................................

	@Autowired private JobBuilderFactory  jobBuilderFactory;

	@Autowired private StepBuilderFactory stepBuilderFactory;

	// constructors
	// ........................................................................

	// methods
	// ........................................................................

	@Bean public RepeatTemplate repeater() {
		
		RepeatTemplate repeater = new RepeatTemplate();
		
		repeater.setCompletionPolicy(new SimpleCompletionPolicy(20));     // <- Limits the number of repeats
		                                                                  //    to 1000, unless the task signals
		                                                                  //    an earlier termination of
		                                                                  //    the step.
		
		repeater.iterate(new TwentyOne());                                // <- The task, which will be
		                                                                  //    iterated, until the number is 21.
		
		return repeater;
		
	}

	@Bean public Step step() {
		
		return
			this.stepBuilderFactory
				.get("step")
				.<Integer, Integer>chunk(5)
				.stepOperations(this.repeater())
				.build();
		
	}

	@Bean public Job job(final Step step) throws Exception {
		
		return
			this.jobBuilderFactory
				.get("job")
				.incrementer(new RunIdIncrementer())
				.start(this.step())
				.build();
		
	}
	
}
